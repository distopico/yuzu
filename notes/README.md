## Yuzu Wiki

https://git.h3cjp.net/H3cJP/yuzu/wiki

https://git.h3cjp.net/H3cJP/yuzu/wiki/Building-for-Linux

https://git.h3cjp.net/H3cJP/yuzu/wiki/Building-for-Windows

## Yuzu Repository Mirror

https://github.com/Lu-Die-Milchkuh/yuzu-will-live

## Biggest yuzu team source code archive (From what I have found)

https://git.zaroz.cloud/nintendo-back-up

## Most updated yuzu repository 

*according to this post and lemmy user*: https://programming.dev/post/10995154

![](https://git.nadeko.net/NINTENSHIT/yuzu-links-and-notes/raw/branch/main/04-214927.png)

https://github.com/zhaobot/yuzu/tree/tx-update-20240301020652

https://git.nadeko.net/NINTENSHIT/yuzu-tx-update (Mirror)

## Software Heritage archive

https://archive.softwareheritage.org/browse/search/?q=github.com%2Fyuzu-emu&with_visit=true&with_content=true

https://lemmy.world/comment/8138249

# Linux Binaries

## Flatpak

https://github.com/flathub/org.yuzu_emu.yuzu (Manifest)

https://git.nadeko.net/NINTENSHIT/flathub_org.yuzu_emu.yuzu/releases (Manifest and Release tar.gz to install using flatpak offline install)

## Arch Linux

https://archlinux.org/packages/extra/x86_64/yuzu/download/ (Not down YET. Arch Maintainers will delete the package from the official packages list some day.)

https://git.nadeko.net/NINTENSHIT/yuzu-ArchLinux-PKGBUILD/releases (64 bits and Arch Linux ARM)

## Debian

http://ftp.us.debian.org/debian/pool/main/y/yuzu/yuzu_0-1335+ds-1.3+b1_arm64.deb (Not down YET. Debian Maintainers will delete the package from the official packages list some day.)

https://git.nadeko.net/NINTENSHIT/yuzu-Debian/releases (64 bits and ARM)

## OpenSUS (OpenSUSE)

https://build.opensuse.org/package/show/openSUSE:Factory/yuzu

https://build.opensuse.org/package/show/Emulators/yuzu

https://git.nadeko.net/NINTENSHIT/yuzu-OpenSUSE/releases